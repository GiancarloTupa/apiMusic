'use strict'

var Songs = require('../models/songs');

function createSong(req, res){
    var song = new Songs();
    song.name = req.body.name;
    song.artist = req.body.artist;
    song.disk = req.body.disk;
    song.gender = req.body.gender;
    song.image = req.body.image;
    song.song = req.body.song;
    song.save(function(err, song){
        if(err){
            res.status(500).send(err)
        }else{
            res.status(200).send(song)
        }
    });
}

function deleteSong(req, res){
    Songs.findByIdAndRemove(req.params.id, function(err, result){
        if(err){
            res.status(500).send(err)
        }else{
            res.status(200).send(result)
        }
    });
}

function insertImageSong(req, res){
    Songs.findByIdAndUpdate(req.params.id, {image:req.files.image.path}, function(err){//tener encuenta la llave en el post
        if(err){
            res.status(500).send(err)
        }else{
            res.status(200).send("Song image: " + req.params.id + " tiene imagen añadida, se añadio la ruta: " + req.files.image.path)
        }
    });
}

function getImageSong(req, res){
    Songs.findById(req.params.id, (err, song)=>{
        const uploads = "C:/Users/gian/Documents/mongoDB/ApiMusica/"
        res.sendFile(uploads+song.image);
    });
}

function getSongs(req, res){
    Songs.find({}, function(err, songs){
        if (err){
            res.status(500).send(err)
        }else{
            res.status(200).send(songs)
        }
    });
}

function getSongforId(req, res){
    Songs.findById(req.user.id, function(err, song){
        if (err){
            res.status(500).send(err)
        }else{
            res.status(200).send(song)
        }
    });
}

function insertFileSong(req, res){
    Songs.findByIdAndUpdate(req.params.id, {song:req.files.song.path}, function(err){//tener encuenta la llave en el post
        if(err){
            res.status(500).send(err)
        }else{
            res.status(200).send("Song: " + req.params.id + " tiene la canción añadida, se añadio la ruta: " + req.files.song.path)
        }
    });
}

function getFileSong(req, res){
    Songs.findById(req.params.id, (err, song)=>{
        const uploads = "C:/Users/gian/Documents/mongoDB/ApiMusica/"
        res.sendFile(uploads+song.song);
    });
}

function getSongName(req, res){
    //console.log(req.params.id);
    Songs.find({name:req.params.name}, function(err, song){
        if (err){
            res.status(500).send(err)
        }else{
            res.status(200).send(song)
        }
    });  
}

module.exports = {
    createSong,
    getSongs,
    deleteSong,
    insertImageSong,
    getImageSong,
    getSongforId,
    getSongName,
    insertFileSong,
    getFileSong
};