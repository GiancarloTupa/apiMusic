'use strict'

var SongsFav = require('../models/songsfav');
var song = require('../models/songs');

function createSongFav(req, res){
    var songsfav = new SongsFav();
    songsfav.idUser = req.user.id;
    songsfav.idSong = req.body.idSong;
    songsfav.save(function(err, songsfav){
        if(err){
            res.status(500).send(err)
        }else{
            res.status(200).send(songsfav)
        }
    });
}
/*
function getSongFav(req, res){
    SongsFav.find({idUser:req.params.id}, function(err, songsfav){
        let cant = songsfav.length;
        if (err){
            res.status(500).send(err)
        }else{
            //for (let x of songsfav){
            for (let i = 0; i < cant; i++) {
                song.find({_id:songsfav[i].idSong}, function(err, result){
                    if(err){
                        res.status(500).send(err)
                    }else{
                        console.log(result);
                        res.status(200).send(result)
                    }
                });
            } 
           // console.log(cant);
          //  res.status(200).send(songsfav)
        }
    }).populate({path:'songs'});
}
*/
function getSongFav(req, res){

    SongsFav.find({idUser:req.user.id}, function(err, songsfav){
        if (err){
            res.status(500).send(err)
        }else{
            // console.log(songsfav);
            let arr_songs_ids = []
            for (let x of songsfav){
                arr_songs_ids.push(x.idSong)
            } 
            song.find({_id: {$in: arr_songs_ids}}, function(err, result){
                if(err){
                    res.status(500).send(err)
                }else{
                  //  console.log(result);
                    res.status(200).send(result)
                }
            });
        }
    });
    // .populate({path:'songs'})
}

function getSongFavforId(req, res){
  //  let id = req.params.id;
//    SongsFav.findById(req.user.id, function(err, songsfav){
      SongsFav.find({$and:[{idSong:req.params.id}, {idUser:req.user.id}]}, function(err, songsfav){
        if (err){
            res.status(500).send(err)
        }else{
           // console.log(songsfav[1].idSong);
           // let id = songsfav[1].idSong;
            console.log(songsfav);
            // song.findById(songsfav.idSong, function(err,result){
            //     if(err){
            //         res.status(500).send(err)  
            //     }else{
            //        console.log(result);
                //    res.status(200).send(result.song)
                //   const uploads = "C:/Users/gian/Documents/mongoDB/ApiMusica/"
                //   res.sendFile(uploads+result.song);
                // }
            // });
            res.status(200).send(songsfav)
            // ----.populate({path:'songs'})
        }
    });
}

function deleteSongFav(req, res){
    SongsFav.findOneAndRemove({$and:[{idSong:req.params.id}, {idUser:req.user.id}]}, function(err, result){
        if (err){
            res.status(500).send(err)
        }else{
            res.status(200).send(result)
        }
    });
}

module.exports = {
    createSongFav,
    getSongFav,
    getSongFavforId,
    deleteSongFav
};