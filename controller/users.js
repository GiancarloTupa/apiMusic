'use strict'

var Users = require('../models/users');
var jwt  = require('../services/jwt');

var bcrypt = require('bcrypt-nodejs');


//crud users
function createUser(req, res){
    var user = new Users();
    user.name = req.body.name;
    user.surname = req.body.surname;
    user.age = req.body.age;
    user.email = req.body.email;
    bcrypt.hash(req.body.password, null, null, function(err, hash){
        if(err){
            res.status(500).send(err)
        }else{
            user.password = hash;
            user.save(function(err, user){
                if(err){
                    res.status(500).send(err)
                }else{
                    res.status(200).send(user)
                }
            });
        }
    });
}

function updateUser(req, res){
    Users.findByIdAndUpdate(req.user.id, req.body, function(err, result){
        if(err){
            res.status(500).send(err)
        }else{
            res.status(200).send(result)
        }
    });
}

function deleteUser(req, res){
    Users.findByIdAndRemove(req.user.id, function(err, result){
        if(err){
            res.status(500).send(err)
        }else{
            res.status(200).send(result)
        }
    });
}

function getUserforId(req, res){
    Users.findById(req.params.id, function(err, user){
        if(err){
            res.status(400).send(err)
        }else{
            res.status(200).send(user)
        }
    });
}

function login(req, res){
    let email = req.body.email;
    let password = req.body.password;
    Users.findOne({email:email}, function(err, user){
        if(user){
            bcrypt.compare(password, user.password, function(err, check){
                if (check) {
                    res.status(200).send({"token":jwt.createToken(user)})
                }else{
                    res.status(400).send("Password incorrecto")
                }
            });
        }else{
            res.status(400).send("No existe el Usuario")
        }
    });
}

module.exports = {
    createUser,
    updateUser,
    deleteUser,
    getUserforId,
    login 
};


