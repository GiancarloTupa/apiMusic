'use strict'

var express = require('express');
var UsersController = require('../controller/users');
var md_auth = require('../middleware/authenticated');
var api = express.Router();

api.post('/user', UsersController.createUser);
api.get('/user/:id', UsersController.getUserforId);
api.put('/user:id', UsersController.updateUser);
api.delete('/user:id', UsersController.deleteUser);
api.post('/login', UsersController.login)

module.exports = api;