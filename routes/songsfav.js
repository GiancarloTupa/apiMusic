'use strict'

var express = require('express');
var SongsFavController = require('../controller/songsfav');
var md_auth = require('../middleware/authenticated');
var api = express.Router();

api.post('/songfav', md_auth.ensureAtuth, SongsFavController.createSongFav);
api.get('/songsfav', [md_auth.ensureAtuth], SongsFavController.getSongFav);//obtener todos las canciones favoritas de un usuario
api.get('/songfav/:id', md_auth.ensureAtuth, SongsFavController.getSongFavforId);//obtener la cancion favorita escogida para la reproduccion
api.delete('/songfav/:id', md_auth.ensureAtuth, SongsFavController.deleteSongFav);

module.exports = api;