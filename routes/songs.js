'use strict'

var express = require('express');
var SongsController = require('../controller/songs');
var md_auth = require('../middleware/authenticated');
var api = express.Router();

var multiparty = require('connect-multiparty');
var md_uploadImage = multiparty({uploadDir: './uploads/imageSong'});
var md_uploadSong = multiparty({uploadDir: './uploads/songs'});

api.post('/song', md_auth.ensureAtuth, SongsController.createSong);
api.get('/songs', SongsController.getSongs);
api.get('/songs/:id', md_auth.ensureAtuth, SongsController.getSongforId);
api.get('/songs/:name', SongsController.getSongName);
api.delete('/song/delete/:id', SongsController.deleteSong);
api.put('/song/insert-image/:id', md_uploadImage , SongsController.insertImageSong);
api.get('/song/get-image/:id', SongsController.getImageSong);
api.put('/song/insert-song/:id', md_uploadSong, SongsController.insertFileSong);
api.get('/song/get-song/:id', SongsController.getFileSong);
module.exports = api;
