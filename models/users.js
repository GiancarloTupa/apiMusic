'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var usersSchema = Schema({
    name: String,
    surname: String,
    age: Number,
    email: String,
    password: String
});

module.exports = mongoose.model('users', usersSchema);