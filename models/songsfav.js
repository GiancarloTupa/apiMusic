'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var songsfavSchema = Schema({
    idUser: {type: mongoose.Schema.Types.ObjectId, ref: 'users'},
    idSong: {type: mongoose.Schema.Types.ObjectId, ref: 'songs'}
   // idUser: String,
    //idSong: String
});

module.exports = mongoose.model('songsfav', songsfavSchema);