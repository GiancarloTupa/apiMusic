'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var songsSchema = Schema({
    name: String,
    artist: String,
    disk: String,
    gender: String,
    image: String,
    song: String
});

module.exports = mongoose.model('songs', songsSchema);