'use strict'

var express = require('express')
var bodyparser = require('body-parser')

var app = express()

//cargado de ruta
var songs_routes = require('./routes/songs')
var user_routes = require('./routes/users')
var songfav_routes = require('./routes/songsfav');

app.use(bodyparser.urlencoded({extended:false}));
app.use(bodyparser.json())

// configurar cabeceras
var cors = require('cors');

// use it before all route definitions
app.use(cors({origin: 'http://localhost:4200'}));

//ruta base
app.use('/api', songs_routes);
app.use('/api', user_routes);
app.use('/api', songfav_routes);

module.exports = app;