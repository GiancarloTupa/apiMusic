'use strict'

var jwt = require('jwt-simple');
var moment = require('moment');//datar fechas
var secret = 'nuestraClave@@';
exports.createToken = function(user){
    var payload = {
        id: user._id,
        iat: moment().unix(),
        exp: moment().add(30, 'days').unix(),
    };
    return jwt.encode(payload, secret)
};